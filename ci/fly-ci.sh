#!/bin/bash

fly -t local set-pipeline --pipeline cloudStreamProducer --config build-local-pipeline.yml \
   --var "private-repo-key=$(cat ~/.ssh/id_rsa)"

fly -t local unpause-pipeline --pipeline cloudStreamProducer
fly -t local trigger-job --job cloudStreamProducer/buildProducer --watch
