package demo;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
// @SpringBootTest(webEnvironment=WebEnvironment.RANDOM_PORT)
@SpringBootTest(webEnvironment = WebEnvironment.MOCK)
@Ignore("too simple to test")
public class MinimalProducerApplicationTests {

    @Test
    public void contextLoads() {
    }

}
