package demo.common.dto;

public class HelloDTO {
   public String greeting = "Hello";
   public String name = "World";

   @Override
   public String toString() {
      return String.format("%s %s!", greeting, name);
   }
}
