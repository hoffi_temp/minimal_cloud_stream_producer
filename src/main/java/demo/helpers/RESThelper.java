package demo.helpers;

import java.lang.reflect.Method;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.ClassPathScanningCandidateComponentProvider;
import org.springframework.core.type.filter.RegexPatternTypeFilter;
import org.springframework.web.bind.annotation.RequestMapping;

public class RESThelper {
    private static final Logger LOG = LoggerFactory.getLogger(RESThelper.class);

    private RESThelper() {
    }

    public static List<String> requestMappings(String prefix, Class<?>... clazzes) throws IllegalStateException {
        List<String> currentClassEndpoints = new LinkedList<>();
        for (Class<?> clazz : clazzes) {
            RequestMapping clazzAnnotationRequestMapping = clazz.getAnnotation(RequestMapping.class);
            if (clazzAnnotationRequestMapping == null) {
                throw new IllegalStateException("class " + clazz.getSimpleName() + " has no @RequestMapping at class level!");
            }
            String[] clazzRequestMappingValues = clazzAnnotationRequestMapping.value();
            String clazzRequestMappingValue = (clazzRequestMappingValues.length == 0 ? "" : clazzRequestMappingValues[0]);
            Method[] methods = clazz.getMethods();
            for (Method method : methods) {
                RequestMapping methodAnnotationRequestMapping = method.getAnnotation(RequestMapping.class);
                if (methodAnnotationRequestMapping == null) {
                    continue;
                }
                String[] values = methodAnnotationRequestMapping.value();
                for (String value : values) {
                    currentClassEndpoints.add(prefix + clazzRequestMappingValue + value);
                }
                if (values.length == 0) {
                    currentClassEndpoints.add(prefix + clazzRequestMappingValue);
                }
            }
            if (currentClassEndpoints.size() == 0) {
                LOG.info("class " + clazz.getSimpleName() + " has no @RequestMapping Methods!");
            }
        }
        return currentClassEndpoints;
    }

    public static List<String> requestMappings(String prefix, String[] classnamePrefixPostfix, Package... packages)
                throws IllegalStateException {

        // create scanner and disable default filters (that is the 'false' argument)
        final ClassPathScanningCandidateComponentProvider classpathScanner = new ClassPathScanningCandidateComponentProvider(false);
        // add include filters which matches all the classes (or use your own)
        String classnamePrefix = "";
        String classnamePostfix = "";
        try {
            classnamePrefix = classnamePrefixPostfix[0];
            classnamePostfix = classnamePrefixPostfix[1];
        } catch (Exception e) {
            // ignore and take defaults
        }
        classpathScanner.addIncludeFilter(new RegexPatternTypeFilter(Pattern.compile(classnamePrefix + ".+" + classnamePostfix)));

        Set<BeanDefinition> clazzes = new HashSet<BeanDefinition>();
        for (Package packag : packages) {
            // get matching classes defined in the package
            Set<BeanDefinition> hereClasses = classpathScanner.findCandidateComponents(packag.getName());
            clazzes.addAll(hereClasses);
        }

        List<String> foundEndpoints = new LinkedList<>();
        for (BeanDefinition bd : clazzes) {
            Class<?> clazz;
            try {
                clazz = Class.forName(bd.getBeanClassName());
            } catch (ClassNotFoundException e) {
                LOG.info(e.getMessage());
                continue;
            }
            RequestMapping clazzAnnotationRequestMapping = clazz.getAnnotation(RequestMapping.class);
            if (clazzAnnotationRequestMapping == null) {
                throw new IllegalStateException("class " + clazz.getSimpleName() + " has no @RequestMapping at class level!");
            }
            List<String> currentClassEndpoints = new LinkedList<>();
            String[] clazzRequestMappingValues = clazzAnnotationRequestMapping.value();
            String clazzRequestMappingValue = (clazzRequestMappingValues.length == 0 ? "" : clazzRequestMappingValues[0]);
            Method[] methods = clazz.getMethods();
            for (Method method : methods) {
                RequestMapping methodAnnotationRequestMapping = method.getAnnotation(RequestMapping.class);
                if (methodAnnotationRequestMapping == null) {
                    continue;
                }
                String[] values = methodAnnotationRequestMapping.value();
                for (String value : values) {
                    currentClassEndpoints.add(prefix + clazzRequestMappingValue + value);
                }
                if (values.length == 0) {
                    currentClassEndpoints.add(prefix + clazzRequestMappingValue);
                }
            }
            if (currentClassEndpoints.size() == 0) {
                LOG.info("class " + clazz.getSimpleName() + " has no @RequestMapping Methods!");
            } else {
                foundEndpoints.addAll(currentClassEndpoints);
            }
        }
        return foundEndpoints;
    }
}
