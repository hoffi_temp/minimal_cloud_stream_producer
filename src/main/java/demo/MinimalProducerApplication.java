package demo;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MinimalProducerApplication implements CommandLineRunner {

    @Value("${my.test.property}")
    String myTestProperty;

    public static void main(String[] args) {
        SpringApplication.run(MinimalProducerApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        System.out.println("myTestProperty: " + myTestProperty);
    }
}
