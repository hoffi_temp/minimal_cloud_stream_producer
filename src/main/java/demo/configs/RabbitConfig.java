package demo.configs;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.connection.RabbitConnectionFactoryBean;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

public class RabbitConfig {
    private static final Logger log = LoggerFactory.getLogger(RabbitConfig.class);

    @Profile({ "local", "docker" }) // only one of these profiles is allowed to be active at the same time
    @Configuration
    public static class RabbitLocalConfig {
        @Value("${spring.rabbitmq.ssl.enabled}")
        boolean rabbitSslEnabled;
        @Value("${spring.rabbitmq.host}")
        String host;

        @Bean
        public ConnectionFactory rabbitConnectionFactory() throws Exception {
            log.info("trying to connect to local RabbitMQ at {}", host);
            CachingConnectionFactory connectionFactory;
            if (rabbitSslEnabled) {
                RabbitConnectionFactoryBean rabbitConnectionFactory = new RabbitConnectionFactoryBean();
                rabbitConnectionFactory.setUseSSL(true);
                // ClassPathResource sslPropertiesLocation = new ClassPathResource("classpath:/rabbitSSL.properties");
                // rabbitConnectionFactory.setSslPropertiesLocation(sslPropertiesLocation);
                rabbitConnectionFactory.afterPropertiesSet();
                connectionFactory = new CachingConnectionFactory(rabbitConnectionFactory.getObject());
                connectionFactory.setAddresses(host);
            } else {
                connectionFactory = new CachingConnectionFactory(host);
            }
            // connectionFactory.setUsername("guest");
            // connectionFactory.setPassword("guest");
            // ccf.setVirtualHost(this.vhost);
            return connectionFactory;
        }
    }
}