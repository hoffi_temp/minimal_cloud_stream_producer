package demo.ws;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import demo.common.dto.HelloDTO;
import demo.helpers.RESThelper;

@RestController
@RequestMapping("/greet")
public class HelloWS {
    private static final Logger LOG = LoggerFactory.getLogger(HelloWS.class);

    @RequestMapping(method = RequestMethod.GET, produces = "application/json")
    public List<String> home() {
        return RESThelper.requestMappings("/greet", HelloWS.class);
    }

    @RequestMapping(value = "/{whom}", method = RequestMethod.GET)
    public HelloDTO greet(@PathVariable("whom") String whom) {
        LOG.info(HelloWS.class.getSimpleName() + ".greet(whom) got called.");
        HelloDTO dto = new HelloDTO();
        dto.name = whom;
        return dto;
    }

    @RequestMapping(value = "/{whom}/{how}", method = RequestMethod.GET)
    public HelloDTO greet(@PathVariable("whom") String whom, @PathVariable("how") String how) {
        LOG.info(HelloWS.class.getSimpleName() + ".greet(whom, how) got called.");
        HelloDTO dto = new HelloDTO();
        dto.name = whom;
        dto.greeting = how;
        return dto;
    }

    @RequestMapping(value = { "/googleMaps" }, method = RequestMethod.GET)
    public String googleMaps() {
        Map<String, String> queryParameters = new HashMap<String, String>();
        queryParameters.put("key", "my_key_here");
        queryParameters.put("latlng", "48.095501,11.518221");
        queryParameters.put("sensor", "false");
        String url = "http://maps.googleapis.com/maps/api/geocode/json?" + "latlng={latlng}&sensor={sensor}";

        HttpHeaders headers = new HttpHeaders();
        headers.set("X-HTTP-Method-Override", "GET");
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

        new HttpEntity<String>(null, headers);

        RestTemplate restclient = new RestTemplate();
        String response = restclient.getForObject(url, String.class, queryParameters);

        return response;
    }
}
