package demo.ws;

import java.util.List;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import demo.helpers.RESThelper;

@RestController
@RequestMapping("/")
public class HomeWS {
    @RequestMapping(method = RequestMethod.GET, produces = "application/json")
    public List<String> home() {
        return RESThelper.requestMappings("", new String[] { "", "WS" }, HomeWS.class.getPackage());
    }
}
