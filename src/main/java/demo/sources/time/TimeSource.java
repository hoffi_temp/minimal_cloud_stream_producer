package demo.sources.time;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.messaging.Source;
import org.springframework.integration.annotation.InboundChannelAdapter;

import demo.common.dto.HelloDTO;
import demo.sources.configs.TimeSourceOptionsMetadata;

@EnableBinding(Source.class)
@EnableConfigurationProperties(TimeSourceOptionsMetadata.class)
public class TimeSource {
    private static final Logger logger = LoggerFactory.getLogger(TimeSource.class);

    @Value("${app.info.instance_index}")
    private String instanceIndex;

    @Autowired
    private TimeSourceOptionsMetadata timeSourceOptionsMetadata;

    @InboundChannelAdapter(value = Source.OUTPUT)
    public HelloDTO timerMessageSource() {
        HelloDTO helloDTO = new HelloDTO();
        helloDTO.name = new SimpleDateFormat(this.timeSourceOptionsMetadata.getFormat()).format(new Date());
        logger.info("[{}]Produced: '{}'", instanceIndex, helloDTO);
        return helloDTO;
        // Message<String> return MessageBuilder.withPayload(helloDTO).setHeader(MessageHeaders.CONTENT_TYPE,
        // "application/json").build();
    }
}
